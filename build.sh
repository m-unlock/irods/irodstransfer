#!/bin/bash
#============================================================================
#title          :iRODS Transfer
#description    :Java app for (large) data transfer from and to iRODS
#author         :Jasper Koehorst
#date           :2022
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# ////////////////////////////////////////////////////////////////////////////////////
$DIR/gradlew build -x test
