package nl.wur.ssb;

import com.beust.jcommander.ParameterException;
import nl.wur.ssb.object.FileObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InterruptedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;

import static nl.wur.ssb.Download.startDownload;

/*
 * Program to easily retrieve and upload data from and to iRODS
 */
public class App {
    static final org.apache.log4j.Logger logger = Logger.getLogger(App.class);
    public static boolean collection = false;
    private static Connection connection;
    public static Integer exitCode = -1;

    public static void main(String[] args) {
        CommandOptions commandOptions = new CommandOptions(args);

        // Setup authentication
        try {
            connection = new Connection(commandOptions);

        // Get or put...
        if (commandOptions.pull) {
            pullData(commandOptions);
        }

        if (commandOptions.push) {
            logger.info("Pushing data to iRODS directory: " + commandOptions.irodsDirectory);
            pushData(commandOptions);
        }
        connection.accessObjectFactory.closeSession(connection.irodsAccount);
        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            // To ensure the connection is always closed in the end
            try {
                logger.error("Catch " + e.getMessage());
                if (connection != null)
                    connection.accessObjectFactory.closeSession(connection.irodsAccount);
            } catch (JargonException ignored) {
                exitCode = 2;
            }
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        // Exit code change if something was not found or failed, but we do not always want to stop the program
        if (exitCode > 0) {
            throw new RuntimeException("Check log for details but some transfers might have failed");
        }
    }

    /**
     *
     * @param commandOptions
     * @throws JargonException
     */
    private static void pushData(CommandOptions commandOptions) throws JargonException, FileNotFoundException {
        // Remote checks
        IRODSFile irodsDirectory = connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory.toString());
        // If there is more than one file given the remote destination should exist and be a folder
        if (commandOptions.files.size() > 1) {
            // Remote path should exists and be a folder
            if (irodsDirectory.exists() && irodsDirectory.isDirectory()) {
                logger.info("Remote irods working directory already exists: " + irodsDirectory);
                // If the input is a folder... This folder needs to be created in irods when multiple values are given
                for (String file : commandOptions.files) {
                    if (new File(file).exists() && new File(file).isDirectory()) {
                        logger.info("Creating irods folder: " + commandOptions.irodsDirectory + "/" + new File(file).getName());
                        connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + new File(file).getName()).mkdirs();
                    }
                }
            } else {
                if (!irodsDirectory.exists() && !irodsDirectory.getParentFile().exists()) {
                    // throw new ParameterException("Current IRODS directory or parent does not exists: " + irodsDirectory.getAbsolutePath());
                    logger.warn("Current IRODS directory or parent does not exists: " + irodsDirectory.getAbsolutePath());
                    irodsDirectory.mkdirs();
                }
                // The folder or the parent should be a folder
                if (!irodsDirectory.isDirectory()) {
                    if (!irodsDirectory.getParentFile().isDirectory()) {
                        throw new ParameterException("Current IRODS path is not a directory: " + irodsDirectory.getAbsolutePath());
                    } else {
                        logger.info("Creating the path: " + irodsDirectory);
                        irodsDirectory.mkdirs();
                    }
                }
            }
        }

        // If there is one file given and the IRODS folder exists a file name needs to be added
        boolean rename = false;
        if (commandOptions.files.size() == 1) {
            // Path points to non existing IRODS file... parent needs to exist
            if (!irodsDirectory.exists() && !irodsDirectory.getParentFile().exists()) {
                logger.info("1");
                logger.info("Path points to non existing irods file and parent folder also does not exists, creating now");
                irodsDirectory.mkdirs();
            } else if (!irodsDirectory.exists() && irodsDirectory.getParentFile().exists() && new File(commandOptions.files.get(0)).isDirectory()) {
                // If parent dir exists but child dir does not exist means its a renaming and we need to create the remote folder
                connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "").mkdirs();
                rename = true;
            } else if (irodsDirectory.exists() && new File(commandOptions.files.get(0)).isDirectory()) {
                // The directory already exists on irods
                if (new File(commandOptions.irodsDirectory).getName().equals(new File(commandOptions.files.get(0)).getName())) {
                    // Folder name in destination is same as folder name provided
                } else {
                    // If the local is a directory and the remote is a existing folder means we need to upload the folder to the new location
                    logger.info("1 Creating iRODS folder: " + commandOptions.irodsDirectory + "/" + new File(commandOptions.files.get(0)).getName());
                    connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + new File(commandOptions.files.get(0)).getName()).mkdirs();
                }
            } else if (irodsDirectory.exists() && new File(commandOptions.files.get(0)).isFile()) {
                logger.info("IRODS folders already exists and ready to accept local file");
                // When directory exists and input is a file then we just upload the file
            } else if (!irodsDirectory.exists() && irodsDirectory.getParentFile().exists() && new File(commandOptions.files.get(0)).isFile()) {
                // If remote directory does not exist and parent does this indicates we need to create a file.
                rename = true;
                logger.info("Renaming " + commandOptions.files.get(0) + " to " + irodsDirectory);
            } else {
                logger.warn("Debug for file " + commandOptions.files.get(0));
                logger.warn("Debug for dir " + irodsDirectory.getAbsolutePath());
                throw new JargonException("What did we miss?");
            }
            // If there is one file given and the IRODS file exists and the force option is !not! used
            if (irodsDirectory.isFile() && !commandOptions.force) {
                throw new ParameterException("IRODS file already exists and overwrite is not enabled (use --force)");
            }
            // If there is one file given and the IRODS file exists and the force option is used
            if (irodsDirectory.isFile() && commandOptions.force) {
                throw new ParameterException("IRODS file already exists and overwrite is not yet available, sorry!");
            }
        }

        // IRODS checks end here... Now for each file given... check push to iRODS

        for (String collection : commandOptions.files) {
            // Java can handle local paths from current directory or full paths
            if (!new File(collection).exists()) {
                throw new DataNotFoundException("Local file / folder does not exists: " + new File(collection).getAbsolutePath());
            }

            // If local file is a folder
            if (new File(collection).isDirectory()) {
                logger.info("Uploading folder: " + collection);
                Upload.uploadCollectionContent(commandOptions, connection, new File(collection));
            } else if (new File(collection).isFile()) {
                // Upload files
                logger.info("Uploading file: " + collection);
                Upload.uploadFile(commandOptions, connection, new File(collection), rename);
            } else {
                throw new JargonException("Object not supported, only files or folders");
            }
        }
    }

    /**
     * Start function to download the files, will perform a number of tests to see if...
     * Local path exists and is a folder (to place the files in)
     * Local path exists and is a file (to place a single file when overwrite is enabled)
     * Local path parent folder exists and file needs to be downloaded and renamed to the target
     * ...?
     *
     * @param commandOptions all authentication and file options
     * @throws JargonException when criteria from above is not met
     */
    private static void pullData(CommandOptions commandOptions) throws JargonQueryException, JargonException, GenQueryBuilderException {
        // If there is more than one file given the local destination should exist and be a folder
        if (commandOptions.files.size() > 1) {
            // Local path should exist and be a folder
            if (commandOptions.localDirectory.exists() && commandOptions.localDirectory.isDirectory()) {
                // All is fine
            } else {
                if (!commandOptions.localDirectory.exists()) {
                    // Check parent folder
                    if (commandOptions.localDirectory.getParentFile().exists()) {
                        commandOptions.localDirectory.mkdir();
                    } else {
                        logger.warn("Current & parent directory does not exists, creating sub directories: " + commandOptions.localDirectory.getAbsolutePath());
                        commandOptions.localDirectory.mkdirs();
                    }
                }
                if (!commandOptions.localDirectory.isDirectory()) {
                    throw new ParameterException("Current path is not a directory: " + commandOptions.localDirectory.getAbsolutePath());
                }
            }
        }

        // If there is one file given and the local folder exists a file name needs to be added
        if (commandOptions.files.size() == 1) {
            // Path points to non-existing local file... parent needs to exist
            if (!commandOptions.localDirectory.exists() && !commandOptions.localDirectory.getParentFile().exists() && !commandOptions.force) {
                throw new ParameterException("Path points to non existing file and parent folder also does not exists, use --force to create parent directories");
            }
            // If there is one file given and the local file exists and the force option is !not! used
            if (commandOptions.localDirectory.isFile() && !commandOptions.force) {
                throw new ParameterException("Local file already exists and overwrite is not enabled (use --force)");
            }
            // If there is one file given and the local file exists and the force option is used
            if (commandOptions.localDirectory.isFile() && commandOptions.force) {
                while (commandOptions.localDirectory.exists()) {
                    commandOptions.localDirectory.delete();
                }
                // throw new ParameterException("Local file already exists and overwrite is not yet available, sorry!");
            }
        }

         // Local checks end here... Now for each file given... check download it from iRODS

        for (String collection : commandOptions.files) {
            // Check if it is a URL and translate to iRODS path
            try {
                // Just a silly rename to validate the url and get the path
                if (collection.startsWith("irods://")) {
                    System.err.println("Renaming irods:// to http:// to bypass url validation and obtain the path");
                    collection = collection.replaceAll("^irods://","http://");
                } else if (collection.matches("^[a-z]://.*")) {

                } else {
                    collection = "file://" + collection;
                }
                URL url = new URL(collection);
                collection = url.getPath();
            } catch (MalformedURLException e) {
                System.err.println(e.getMessage());
            }

            IRODSFile irodsCollection = connection.fileFactory.instanceIRODSFile(collection);

            // Remote checks
            if (!irodsCollection.exists()) {
                logger.error("Remote file / folder does not exists: " + collection);
            }

            // Download folders
            if (!irodsCollection.exists()) {
                logger.error("Irods collection does not exist, cannot download " + irodsCollection.getAbsolutePath());
            } else if (irodsCollection.isDirectory()) {
                App.collection = true;
                try {
                    Download.downloadCollectionContent(commandOptions, connection, new File(collection));
                } catch (InterruptedIOException e) {
                    logger.info(e.getMessage());
                }
            } else {
                App.collection = false;
                // Download files
                FileObject fileObject = new FileObject();
                fileObject.setPath(irodsCollection.getPath());
                fileObject.setSize(irodsCollection.length());

                DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
                ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsCollection);
                fileObject.setChecksum(remoteChecksumValue.getBase64ChecksumValue());

                try {
                    startDownload(commandOptions, connection, fileObject);
                } catch (InterruptedIOException e) {
                    logger.error("This exception should not be thrown...");
                }
            }
        }
    }
}
