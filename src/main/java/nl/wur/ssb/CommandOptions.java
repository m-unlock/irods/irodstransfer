package nl.wur.ssb;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
java -jar irods.jar --local /home/hoekorst/local --irods /tempZone/home/koehorst/non-existing --push /tmp/somethingelse.txt foo.txt
java -jar irods.jar --local /home/hoekorst/local --irods /tempZone/home/koehorst/non-existing --pull /tempZone/home/colleague/somethingelse.txt foo.txt
java -jar irods.jar --local /home/hoekorst/local --irods /tempZone/home/koehorst/non-existing --pull /tempZone/home/colleague/
java -jar irods.jar --local /home/hoekorst/local --irods /tempZone/home/ --push colleague/
java -jar irods.jar --local /home/hoekorst/local --irods /tempZone/file.txt --push not-file.txt
 */

@Parameters(commandDescription = "Available options: ")
public class CommandOptions {

    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    @Parameter(names = {"--pull", "-pull"}, description = "Pull data from irods")
    public boolean pull;

    @Parameter(names = {"--push", "-push"}, description = "Push data to irods")
    public boolean push;

    @Parameter(names = {"--irods", "-irods"}, description = "Destination directory for irods (System variable: irodsCwd)")
    public String irodsDirectory = System.getenv("irodsCwd");

    @Parameter(names = {"--local", "-local"}, description = "Local working directory")
    public File localDirectory = new File(System.getProperty("user.dir"));

    @Parameter(names = {"--flatten", "-flatten"}, description = "Discard the folder structure relative to local directory")
    public boolean flatten;

    // Force overwrite
    @Parameter(names = {"--force", "-force"}, description = "Be careful, overwrites existing files and creates parent directories")
    public boolean force;

    // List of files
    @Parameter(names = {"--files","--folders", "--values", "-values"}, variableArity = true, description = "List of files / folders you want to sent or retrieve")
    public List<String> files = new ArrayList<>();

    // Default irods settings
    @Parameter(names = {"--username", "-username"}, description = "Username for iRODS authentication (System variable: irodsUserName)")
    public String username = System.getenv("irodsUserName");

    @Parameter(names = {"--password","-password"}, description = "Password for iRODS authentication (System variable: irodsPassword)")
    public String password = System.getenv("irodsPassword");

    @Parameter(names = {"--host","-host"}, description = "Host for iRODS authentication (System variable: irodsHost)")
    public String host = System.getenv("irodsHost");

    @Parameter(names = {"--port","-port"}, description = "Port for iRODS authentication (System variable: irodsPort)")
    public String port = System.getenv("irodsPort");

    @Parameter(names = {"--zone","-zone"}, description = "Zone for iRODS authentication (System variable: irodsZone)")
    public String zone = System.getenv("irodsZone");

    @Parameter(names = {"--threads","-threads"}, description = "Number of threads to use for parallel transfer")
    public int threads = 5;

    @Parameter(names = {"--authentication","-authentication"}, description = "Authentication scheme for iRODS authentication, e.g. password (System variable: irodsAuthScheme)")
    String authentication = System.getenv("irodsAuthScheme");

    @Parameter(names = {"--sslpolicy", "-sslpolicy"}, description = "SSL Negotiation policy e.g. CS_NEG_REQUIRE, CS_NEG_REFUSE (System variable: irodsSSL)")
    public String sslPolicyString = System.getenv("irodsSSL");

    public CommandOptions(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            if (this.push && this.pull) {
                throw new ParameterException("Push and Pull are both defined which is not allowed...");
            }

            if (!this.push && !this.pull) {
                throw new ParameterException("--push or --pull needs to be set...");
            }

            // if any of the options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }

            // Making the array list unique (cwl issue)
            files = files.stream().distinct().collect(Collectors.toList());
            // System.err.println(StringUtils.join(files, " "));

        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
