package nl.wur.ssb;

import org.irods.jargon.core.connection.*;
import org.irods.jargon.core.connection.auth.AuthResponse;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.*;

public class Connection {
    public final IRODSFileSystem irodsFileSystem;
    public final IRODSAccount irodsAccount;
    public final IRODSAccessObjectFactory accessObjectFactory;
    public final IRODSFileFactory fileFactory;
    private static final Logger log = Logger.getLogger(Connection.class.getName());

    public Connection(CommandOptions commandOptions) throws JargonException {
        // Initialize account object
        irodsAccount = IRODSAccount.instance(
                commandOptions.host,
                Integer.parseInt(commandOptions.port),
                commandOptions.username,
                commandOptions.password,
                "",
                commandOptions.zone,
                "");

        SettableJargonPropertiesMBean jargonProperties = new SettableJargonProperties();
        jargonProperties.setChecksumEncoding(ChecksumEncodingEnum.SHA256);

        // Set SSL negotiation SSL
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
        if (commandOptions.sslPolicyString.matches("CS_NEG_REQUIRE")) {
            sslPolicy = CS_NEG_REQUIRE;
        } else if (commandOptions.sslPolicyString.matches("CS_NEG_REFUSE")) {
            sslPolicy = CS_NEG_REFUSE;
        } else if (commandOptions.sslPolicyString.matches("CS_NEG_DONT_CARE")) {
            sslPolicy = CS_NEG_DONT_CARE;
        } else if (commandOptions.sslPolicyString.matches("CS_NEG_FAILURE")) {
            sslPolicy = CS_NEG_FAILURE;
        } else {
            throw new JargonException("SSL policy not recognised: " + commandOptions.sslPolicyString);
        }

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        if (commandOptions.authentication.equalsIgnoreCase("pam"))
            irodsAccount.setAuthenticationScheme(AuthScheme.PAM);
        if (commandOptions.authentication.equalsIgnoreCase("kerbos"))
            irodsAccount.setAuthenticationScheme(AuthScheme.KERBEROS);
        if (commandOptions.authentication.equalsIgnoreCase("gsi"))
            irodsAccount.setAuthenticationScheme(AuthScheme.GSI);
        if (commandOptions.authentication.equalsIgnoreCase("standard"))
            irodsAccount.setAuthenticationScheme(AuthScheme.STANDARD);

        irodsFileSystem = IRODSFileSystem.instance();
        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();
        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
    }
}
