package nl.wur.ssb;

import nl.wur.ssb.object.FileObject;
import org.apache.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class Download {
    static final org.apache.log4j.Logger logger = Logger.getLogger(Download.class);

    /**
     * Downloads the content of a collection (folder)
     *
     * @param commandOptions general arguments
     * @param collection     the specific folder to be downloaded
     * @throws JargonException when collection does not exist
     */
    public static void downloadCollectionContent(CommandOptions commandOptions, Connection connection, File collection) throws JargonQueryException, JargonException, GenQueryBuilderException, InterruptedIOException {
        // Get all files from this directory including subdirectories

        // Remote folder exists as checked in previous function...
        IRODSFile irodsCollection = connection.fileFactory.instanceIRODSFile(String.valueOf(collection));
        if (!irodsCollection.exists()) {
            throw new JargonException("File/Folder does not exist " + collection);
        }

        // Perform a query?
        logger.info("Retrieving " + collection.getAbsolutePath());
        ArrayList<FileObject> folderObjects = IQuest.getFolders(connection, collection);

        for (int i = 0; i < folderObjects.size(); i++) {
            logger.info("Processing " + i + " of " + folderObjects.size() + " folders");
            ArrayList<FileObject> fileObjects = IQuest.getFiles(connection, new File(folderObjects.get(i).getPath()));
            System.err.print(fileObjects.size() + " files total\r");
            for (FileObject fileObject : fileObjects) {
                startDownload(commandOptions, connection, fileObject);
            }
        }
    }

    public static void startDownload(CommandOptions commandOptions, Connection connection, FileObject fileObject) throws JargonException, InterruptedIOException {
        // Keep downloading upon failures
        boolean download = false;
        int attempt = 0;
        while (!download) {
            attempt = attempt + 1;
            if (attempt > 3) {
                throw new org.irods.jargon.core.exception.JargonException("Transfer failed after 3 attempts");
            }
            try {
                Download.downloadFile(commandOptions, connection, fileObject);
                download = true;
            } catch (JargonException e) {
                if (e.getMessage().contains("No access to item in catalog")) {
                    logger.error(e.getMessage());
                    download = true;
                } else {
                    logger.error("Download failed, retry attempt..." + e.getMessage());
                }
            } catch (FileNotFoundException e) {
                logger.error("File not found on irods " + fileObject.getPath());
                download = true;
            }
        }
    }

    public static void downloadFile(CommandOptions commandOptions, Connection connection, FileObject fileObject) throws JargonException, FileNotFoundException, InterruptedIOException {

        DataTransferOperations dataTransferOperationsAO = connection.accessObjectFactory.getDataTransferOperations(connection.irodsAccount);

        // Set file to local directory
        File localFile = new File(commandOptions.localDirectory +"/"+ new File(fileObject.getPath()).getName());
        if (!commandOptions.flatten) {
            // Add path of folder to localDirectory,
            String newPath = commandOptions.localDirectory.getAbsolutePath() + "/" + fileObject.getPath();
            newPath = newPath.replaceAll("/+", "/");
            localFile = new File(newPath);
            if (App.collection && localFile.exists() && !commandOptions.force) {
                // Skip the entire collection otherwise use force
                throw new java.io.InterruptedIOException("Collection " + localFile.getParentFile() + " already exists. To force collection download use --force");
            }
        }

        // If the file or folder exists
        if (localFile.exists()) {
            // If the location points to a file
            if (localFile.isFile()) {
                // Perform a simple file size check by default
                if (localFile.length() != fileObject.getSize()) {
                    boolean deleted = localFile.delete();
                    if (deleted) {
                        logger.error("File size does not match, removed file from system: " + localFile);
                    } else {
                        throw new JargonException("Deleting file failed: " + localFile);
                    }
                } else if (commandOptions.force) {
                    // Get local HASH
                    LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                    SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                    ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                    // Get remote hash
                    String remoteChecksumValue = fileObject.getChecksum();
                    if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue)) { // .getBase64ChecksumValue())) {
                        logger.debug("Same checksum, not going to overwrite");
                        return;
                    } else {
                        logger.info("Remove local file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                        logger.info("Does not match checksum of " + fileObject.getPath() + " " + remoteChecksumValue); // .getBase64ChecksumValue());
                        localFile.delete();
                    }
                } else {
                    logger.info("Skipping " + localFile);
                }
            }
        }

        // THE ACTUAL TRANSFER

        // Disables the logger for the transfer as it easily gives thousands of lines... and perform the transfer
        org.apache.log4j.Logger.getLogger("org.irods.jargon.core.transfer").setLevel(org.apache.log4j.Level.INFO);
        if (!localFile.exists()) {
            if (!localFile.getParentFile().exists()) {
                logger.debug("Creating directory " + localFile.getParentFile());
                localFile.getParentFile().mkdirs();
            }
            logger.info("Downloading file from " + fileObject.getPath() + " to " + localFile.getAbsolutePath());
            try {
                IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(fileObject.getPath());
                // System.err.println(irodsFile.canWrite());
                StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
                TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
                TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                // Force no checksum when anonymous...

                if (commandOptions.username.contains("anonymous")) {
                    transferOptions.setComputeAndVerifyChecksumAfterTransfer(false);
                    transferOptions.setComputeChecksumAfterTransfer(false);
                }
                transferOptions.setIntraFileStatusCallbacks(true);
                transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
                transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
                transferOptions.setMaxThreads(commandOptions.threads);
                logger.debug("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
                logger.debug("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());
                defaultTransferControlBlock.setTransferOptions(transferOptions);

                dataTransferOperationsAO.getOperation(irodsFile, localFile, statusCallbackListener, defaultTransferControlBlock);
                logger.debug("Transfer completed");
            } catch (JargonException e) {
                if (localFile.exists()) {
                    localFile.delete();
                }
                throw new JargonException("Download failed, removed local file: " + localFile);
            }

            // Always performs size check
            if (localFile.length() != fileObject.getSize()) {
                // Delete local file
                while (localFile.exists()) {
                    localFile.delete();
                }
                throw new JargonException("Byte size incorrect, download failed");
            }

            // Size limit to 50gb for checksum when available
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            if (dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(fileObject.getPath()) != null || localFile.length() / (1024 * 1024 * 1024) < 50) {
                // Checksum test!
                LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                // Get remote hash
                ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(fileObject.getPath());
                if (remoteChecksumValue == null) {
                    // No checksum possible
                    logger.warn("No checksum data available but size match...");
                    return;
                }
                if (!localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                    logger.error("Does not match checksum of " + fileObject.getPath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                    localFile.delete();
                    logger.error("Local checksum:" + localChecksumValue.getBase64ChecksumValue());
                    logger.error("Remote checksum:" + remoteChecksumValue.getBase64ChecksumValue());
                    throw new JargonException("Download failed! Incomplete / different checksum detected " + localFile);

                }
            }
        }
    }
}
