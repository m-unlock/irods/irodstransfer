package nl.wur.ssb;

import nl.wur.ssb.object.FileObject;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.query.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class IQuest {
    static final org.apache.log4j.Logger logger = Logger.getLogger(IQuest.class);

    public static ArrayList<FileObject> getFiles(Connection connection, File irodsFolder) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Get unprocessed files
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtain all TTL files in /Unprocessed folder

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, irodsFolder + "%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_DATA_CHECKSUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(99999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            logger.error("No results found with " + irodsFolder);
            App.exitCode = 2;
            return new ArrayList<>();
        } else {
            System.err.println("Found " + irodsQueryResultSetResults.size() + " files...\r");
        }

        HashSet<FileObject> filePaths = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            String checksum = irodsQueryResultSetResult.getColumn(3);
            String size = irodsQueryResultSetResult.getColumn(2);

            FileObject fileObject = new FileObject();
            fileObject.setPath(path);
            fileObject.setSize(Long.valueOf(size));
            if (checksum.length() >0)
                fileObject.setChecksum(checksum.split(":")[1]);

            filePaths.add(fileObject);
            System.err.print(filePaths.size() + " files added\r");
        }

        ArrayList<FileObject> fileArrayList = new ArrayList<>();
        fileArrayList.addAll(filePaths);
        return fileArrayList;
    }

    public static ArrayList<FileObject> getFolders(Connection connection, File irodsFolder) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Get unprocessed files
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtain all TTL files in /Unprocessed folder

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, irodsFolder + "%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(99999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            logger.error("No results found with " + irodsFolder);
            App.exitCode = 2;
            return new ArrayList<>();
        } else {
            System.err.println("Found " + irodsQueryResultSetResults.size() + " folders...\r");
        }

        HashSet<FileObject> filePaths = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0); //  + "/" + irodsQueryResultSetResult.getColumn(1);
            FileObject fileObject = new FileObject();
            fileObject.setPath(path);
            filePaths.add(fileObject);
            System.err.print(filePaths.size() + " folders added\r");
        }

        ArrayList<FileObject> fileArrayList = new ArrayList<>();
        fileArrayList.addAll(filePaths);
        return fileArrayList;
    }
}
