package nl.wur.ssb;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.transfer.TransferStatus;
import org.irods.jargon.core.transfer.TransferStatus.TransferState;
import org.irods.jargon.core.transfer.TransferStatusCallbackListener;

public class StatusCallbackListener implements TransferStatusCallbackListener {

    static final org.apache.log4j.Logger logger = Logger.getLogger(Download.class);

    private int successCallbackCount = 0;
    private int errorCallbackCount = 0;
    private String lastSourcePath = "";
    private String lastTargetPath = "";
    private String lastResource = "";
    private TransferStatusCallbackListener.CallbackResponse forceOption = TransferStatusCallbackListener.CallbackResponse.NO_FOR_ALL;
    private long bytesReportedIntraFileCallbacks = 0L;
    private int numberIntraFileCallbacks = 0;
    private final List<TransferStatus> errorCallbacks = new ArrayList<>();
    private long transfered = 0L;

    public List<TransferStatus> getErrorCallbacks() {
        return errorCallbacks;
    }

    @Override
    public FileStatusCallbackResponse statusCallback(final TransferStatus transferStatus) throws JargonException {
        // System.err.println(transferStatus);
        // Reporting per GB
        long gbs = Math.round((double) transferStatus.getBytesTransfered() / (1024 * 1024 * 1024));
        if (transfered < gbs) {
            long totalSize = transferStatus.getTotalSize() / (1024 * 1024 * 1024);
            if (totalSize == 0)
                totalSize = 1;
            System.err.print("\rTransfer status: " + transfered + "GB of " + totalSize + "GB " + String.format("%.2f", ((double) transfered / (double) totalSize) * 100.0) + "%");
            transfered = gbs;
        } else if (transfered == 0) {
            long totalSize = transferStatus.getTotalSize() / (1024 * 1024 * 1024);
            if (totalSize > 1 && transfered == 0) {
                System.err.print("\rTransfer status: " + transfered + "GB of " + totalSize + "GB " + String.format("%.2f", ((double) transfered / (double) totalSize) * 100.0) + "%");
            }
        }
        if (transferStatus.isIntraFileStatusReport()) {
            numberIntraFileCallbacks++;
            bytesReportedIntraFileCallbacks = transferStatus.getBytesTransfered();
        } else if (transferStatus.getTransferState() == TransferState.FAILURE) {
            logger.error("Transfer state to failure " + errorCallbackCount);
            errorCallbackCount++;
        } else if (transferStatus.getTransferState() == TransferState.IN_PROGRESS_START_FILE) {
            // ignored
        } else {
            successCallbackCount++;
            lastSourcePath = transferStatus.getSourceFileAbsolutePath();
            lastTargetPath = transferStatus.getTargetFileAbsolutePath();
            lastResource = transferStatus.getTargetResource();
        }

        if (transferStatus.getTransferException() != null) {
            errorCallbacks.add(transferStatus);
            logger.error("Transfer status exception: " + transferStatus.getTransferException().getMessage());
            throw new JargonException("Transfer stopped...");
        }
        return FileStatusCallbackResponse.CONTINUE;

    }

    public int getSuccessCallbackCount() {
        return successCallbackCount;
    }

    public void setSuccessCallbackCount(final int successCallbackCount) {
        this.successCallbackCount = successCallbackCount;
    }

    public int getErrorCallbackCount() {
        return errorCallbackCount;
    }

    public void setErrorCallbackCount(final int errorCallbackCount) {
        this.errorCallbackCount = errorCallbackCount;
    }

    public String getLastSourcePath() {
        return lastSourcePath;
    }

    public String getLastTargetPath() {
        return lastTargetPath;
    }

    public String getLastResource() {
        return lastResource;
    }

    @Override
    public void overallStatusCallback(final TransferStatus transferStatus) {
        if (transferStatus.getTransferState() == TransferState.FAILURE) {
            errorCallbackCount++;
        } else {
            successCallbackCount++;
        }
        lastSourcePath = transferStatus.getSourceFileAbsolutePath();
        lastTargetPath = transferStatus.getTargetFileAbsolutePath();
        lastResource = transferStatus.getTargetResource();
    }

    /**
     * @return the bytesReportedIntraFileCallbacks
     */
    public long getBytesReportedIntraFileCallbacks() {
        return bytesReportedIntraFileCallbacks;
    }

    /**
     * @return the numberIntraFileCallbacks
     */
    public int getNumberIntraFileCallbacks() {
        return numberIntraFileCallbacks;
    }

    @Override
    public CallbackResponse transferAsksWhetherToForceOperation(final String irodsAbsolutePath,
                                                                final boolean isCollection) {
        return forceOption;
    }

    public CallbackResponse getForceOption() {
        return forceOption;
    }

    public void setForceOption(final CallbackResponse forceOption) {
        this.forceOption = forceOption;
    }
}