package nl.wur.ssb;

import org.apache.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.FileNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.exception.OverwriteException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;

public class Upload {
    static final org.apache.log4j.Logger logger = Logger.getLogger(Upload.class);

    public static void uploadCollectionContent(CommandOptions commandOptions, Connection connection, File localFolder) throws JargonException {
        uploadCollectionContent(commandOptions, connection, localFolder, null);
    }

    public static void uploadCollectionContent(CommandOptions commandOptions, Connection connection, File localFolder, File targetFolder) throws JargonException {
        // Specify the irods target
        if (targetFolder == null) {
            if (new File(commandOptions.irodsDirectory).getName().equals(localFolder.getName())) {
                targetFolder = new File(commandOptions.irodsDirectory);
            } else{
                targetFolder = new File(commandOptions.irodsDirectory + "/" + localFolder.getName());
            }
        } else {
            targetFolder = new File(targetFolder + "/" + localFolder.getName());
        }

        if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFolder.getName()).exists()) {
            if (!connection.fileFactory.instanceIRODSFile(targetFolder.getAbsolutePath()).exists()) {
                connection.fileFactory.instanceIRODSFile(targetFolder.getAbsolutePath()).mkdirs();
            }
        }

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        for (File localFile : localFolder.listFiles()) {
            try {
                if (localFile.isDirectory()) {
                    logger.debug("Directory detected");
                    uploadCollectionContent(commandOptions, connection, localFile, targetFolder);
                } else {
                    // Upload file function?
                    logger.debug("Uploading file: " + localFile);
                    commandOptions.irodsDirectory = targetFolder.getAbsolutePath();
                    Upload.uploadFile(commandOptions, connection, localFile, false);

//                    IRODSFile destFile = connection.fileFactory.instanceIRODSFile(targetFolder + "/" + localFile.getName());
//                    StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
//                    TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
//                    TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
//                    transferOptions.setIntraFileStatusCallbacks(true);
//
//                    if (commandOptions.force) {
//                        transferOptions.setForceOption(TransferOptions.ForceOption.USE_FORCE);
//                    } else if (destFile.exists()) {
//                      logger.warn("Skipping as destination file already exists and no force option is used " + destFile.getAbsolutePath());
//                      continue;
//                    } else {
//                        transferOptions.setForceOption(TransferOptions.ForceOption.NO_FORCE);
//                    }
//
//                    logger.debug("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
//                    logger.debug("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());
//
//                    transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
//                    transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
//                    defaultTransferControlBlock.setTransferOptions(transferOptions);
//                    dataTransferOperationsAO.putOperation(localFile, destFile, statusCallbackListener, defaultTransferControlBlock);
//                    logger.info("Transfer completed of " + localFile.getName());
                }
            } catch (JargonException e) {
                logger.error(e.getMessage());
            } catch (java.io.FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void uploadFile(CommandOptions commandOptions, Connection connection, File localFile) throws JargonException, java.io.FileNotFoundException {
        uploadFile(commandOptions, connection, localFile, false);
    }

    public static void uploadFile(CommandOptions commandOptions, Connection connection, File localFile, boolean rename) throws JargonException, java.io.FileNotFoundException {

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // If path does not exist but parent does this means a file rename

        // If this is an existing directory, the filename needs to be appended to it
//        if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "").isDirectory()) {
//            if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory.getParentFile() + "").isDirectory()) {
//                throw new ResourceDoesNotExistException("The destination is not an existing folder: " + commandOptions.irodsDirectory);
//            }
//        }

        if (connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFile.getName()).exists()) {
            if (connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFile.getName()).isDirectory()) {
                throw new FileNotFoundException("Folder already exists with the name of the file you are trying to upload");
            }

            if (!commandOptions.force) {
                logger.warn(localFile.getName() + " already exists");
                return;
            }
        }
        // Default is to keep the name
        String target = commandOptions.irodsDirectory + "/" + localFile.getName();

        logger.info("Uploading " + localFile.getName() + " to " + target);

        // Unless we need to rename it
        if (rename) {
            target = commandOptions.irodsDirectory;
        }
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(target);

        StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
        TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
        TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
        transferOptions.setIntraFileStatusCallbacks(true);
        transferOptions.setUseParallelTransfer(true);
        transferOptions.setMaxThreads(commandOptions.threads);
        if (commandOptions.force) {
            transferOptions.setForceOption(TransferOptions.ForceOption.USE_FORCE);
        } else if (irodsFile.exists()) {
            logger.info("Skipping as destination file already exists and no force option is used " + irodsFile.getAbsolutePath());
            return;
        } else {
            transferOptions.setForceOption(TransferOptions.ForceOption.NO_FORCE);
        }

        logger.debug("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
        logger.debug("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());

//        transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
//        transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
//        defaultTransferControlBlock.setTransferOptions(transferOptions);
//        System.err.println(localFile + " uploading to " + destFile);
//        dataTransferOperationsAO.putOperation(localFile, destFile, statusCallbackListener, defaultTransferControlBlock);
//        logger.info("Transfer completed of " + localFile.getName());

//        logger.info("Downloading file from " + irodsFile + " to " + localFile.getAbsolutePath());
        try {
//            StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
//            TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
//            TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
//            transferOptions.setIntraFileStatusCallbacks(true);
            transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
            transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
            logger.debug("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
            logger.debug("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());
            defaultTransferControlBlock.setTransferOptions(transferOptions);
//             dataTransferOperationsAO.putOperation(localFile, irodsFile, statusCallbackListener, defaultTransferControlBlock);
            dataTransferOperationsAO.putOperation(localFile, irodsFile, statusCallbackListener, defaultTransferControlBlock);
            logger.debug("Transfer completed");
        } catch (JargonException e) {
            if (irodsFile.exists()) {
                irodsFile.delete();
            }
            throw new JargonException("Download failed, remove remote file: " + irodsFile);
        }

        // Size limit to 50gb for checksum
        if (localFile.length() / (1024 * 1024 * 1024) > 50) {
            if (localFile.length() != irodsFile.length()) {
                // Delete local file
                while (irodsFile.exists()) {
                    irodsFile.delete();
                }
                throw new JargonException("Byte size incorrect, download failed");
            } else {
                logger.info("File is too large for checksum validation but byte validation passed");
            }
        } else {
            // Checksum test!
            LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
            SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
            ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
            // Get remote hash
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
            if (!localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                logger.error("Does not match checksum of " + irodsFile.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                irodsFile.delete();
                throw new JargonException("Upload failed! Incomplete / different checksum detected " + irodsFile + " " + remoteChecksumValue.getBase64ChecksumValue());
            }
        }
    }
}
