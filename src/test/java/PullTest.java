import nl.wur.ssb.App;
import nl.wur.ssb.CommandOptions;
import nl.wur.ssb.Connection;
import org.apache.commons.io.FileUtils;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.FileAlreadyExistsException;
import java.util.Map;
import java.util.Scanner;

public class PullTest {

    // Uploads folders to a location
    @Before
    public void prepareFiles() throws IOException, JargonException, GenQueryBuilderException, JargonQueryException {
        File outputFolder = new File("./src/test/resources/output");
        while (outputFolder.exists()) {
            FileUtils.deleteDirectory(outputFolder);
        }
        outputFolder.mkdirs();

        String[] args = {"-values", "./src/test/resources/upload/firstFolder", "./src/test/resources/upload/secondFolder", "--push", "--irods", "/unlock/home/koehorst/"};
        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);

        if (connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder/TEST1.txt").isFile()) return;

        App.main(args);

        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder/TEST1.txt").isFile());
    }

    @Test
    public void testBlanco() {

    }

    // - Pull an irods file to an existing local location (create)
    @Test
    public void test1() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/TEST1.txt", "--pull", "--irods", "/unlock/home/koehorst"};
        App.main(args);

        Assert.assertEquals(true, new File("./TEST1.txt").isFile());
        new File("./TEST1.txt").delete();
    }

    // - Pull an irods file to an existing local location (create)
    @Test
    public void test1Existing() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        new File("./TEST1.txt").delete();

        String[] args = {"-values", "./firstFolder/TEST1.txt", "--pull", "--irods", "/unlock/home/koehorst"};
        App.main(args);
        // Run again to download file to local
        String[] args2 = {"-values", "./firstFolder/TEST1.txt", "--pull", "--irods", "/unlock/home/koehorst", "--force"};
        App.main(args2);

        Assert.assertEquals(true, new File("./TEST1.txt").isFile());
        new File("./TEST1.txt").delete();
    }

    // - Pull a remote file to a non-existing file location (rename)
    @Test
    public void test2() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/TEST1.txt", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./TEST2.txt"};
        App.main(args);

        Assert.assertEquals(true, new File("./TEST2.txt").isFile());
        new File("./TEST2.txt").delete();
    }

    // - Pull multiple remote files to an existing local folder location
    @Test
    public void test3() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/TEST1.txt", "./secondFolder/TEST2.txt", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/TEST1.txt").isFile());
    }

    // - Pull multiple remote files to a non-existing local folder location (create path)
    @Test
    public void test4() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/TEST1.txt", "./secondFolder/TEST2.txt", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/newFolder"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/newFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/newFolder/TEST2.txt").isFile());
    }

    // - Pull a single remote folder to an existing local location (create)
    @Test
    public void test5() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/firstFolder/TEST2.txt").isFile());
    }

    // - Pull a single remote folder to a non-existing local folder (rename)
    @Test
    public void test6() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/somethingNew"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/somethingNew/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/somethingNew/firstFolder/TEST2.txt").isFile());
    }

    // - Pull multiple folders to a local existing folder location (create)
    @Test
    public void test7() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/", "./secondFolder/", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/secondFolder/TEST2.txt").isFile());
    }

    // - Pull multiple folders to a local non-existing folder location (create)
    @Test
    public void test8() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./firstFolder/", "./secondFolder/", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/nonExisting"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/nonExisting/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/nonExisting/secondFolder/TEST2.txt").isFile());
    }

    // - Pull from URLs to a local non existing folder
//    @Test
    public void test9() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values",
                "https://wurk-resc.irods.surfsara.nl/unlock/Projects/P_NWO_unlock_test/I_Investigation_Identifier/S_Study_Identifier/O_ObservationUnit_1/DNA/A_DNATEST_Pacbio/Unprocessed/Unlock_DNA-test_PB.bam",
                "irods://wurk-resc.irods.surfsara.nl/unlock/Projects/P_NWO_unlock_test/I_Investigation_Identifier/S_Study_Identifier/O_ObservationUnit_1/DNA/A_DNATEST_illumina/Unprocessed/Unlock_DNA-test_1.fq.gz",
                "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output/nonExisting2"};
        App.main(args);

        Assert.assertEquals(true, new File("./src/test/resources/output/nonExisting2/Unlock_DNA-test_PB.bam").isFile());
        Assert.assertEquals(true, new File("./src/test/resources/output/nonExisting2/Unlock_DNA-test_1.fq.gz").isFile());
    }

//    @Test
    public void test10() throws FileAlreadyExistsException, JargonException, FileNotFoundException, GenQueryBuilderException, JargonQueryException {
//        setEnv();
        String[] args = {"-values",
                "https://wurk-resc.irods.surfsara.nl/unlock/RawData/Projects/P_Transgenerational_nutrition_-_NWO", "--pull", "--irods", "/unlock/home/koehorst", "--local", "./src/test/resources/output"};
        App.main(args);
    }

//    @Test
    public void test11() throws FileNotFoundException, FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException {
//        setEnv();
        String[] args = {"--pull", "--local", "./unlock/References/Databases/KRAKEN2_STANDARD_20200724", "--files", "/unlock/References/Databases/KRAKEN2_STANDARD_20200724", "--force"};
        App.main(args);
    }

    @Test
    public void testHuge() throws FileAlreadyExistsException, JargonQueryException, JargonException, FileNotFoundException, GenQueryBuilderException {
        String[] args = {"--pull", "--files", "/unlock/projects/PRJ_UNLOCK/INV_TEST/STU_Zymo-mock-dataset/OBS_Zymo-EVEN-BB-SN/SAM_Zymo-PromethION-EVEN/dna/ASY_Zymo-PromethION-EVEN-BB-SN/unprocessed/Zymo-PromethION-EVEN-BB-SN.fq.gz"};
        App.main(args);
    }

    @Test
    public void testFolder() throws FileAlreadyExistsException, JargonQueryException, JargonException, FileNotFoundException, GenQueryBuilderException {
        String[] args = {"--local", "/", "--preserve", "--pull", "--files", "/unlock/projects/PRJ_IBISBA/INV_COCULTURE/references/KRAKEN_COCULTURE"};
        App.main(args);
    }

//    @Before
//    public void setEnv() throws FileNotFoundException {
//        Scanner scanner = new Scanner(new File("config.txt"));
//        while (scanner.hasNextLine()) {
//            String[] line = scanner.nextLine().split("\\s+");
//            setEnv(line[0], line[1]);
//        }
//    }

    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }
}
