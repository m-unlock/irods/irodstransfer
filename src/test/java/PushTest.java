import nl.wur.ssb.App;
import nl.wur.ssb.CommandOptions;
import nl.wur.ssb.Connection;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.nio.file.FileAlreadyExistsException;
import java.util.Map;
import java.util.Scanner;

public class PushTest {

    @Before
    public void setEnv() throws FileNotFoundException {
//        Scanner scanner = new Scanner(new File("config.txt"));
//        while (scanner.hasNextLine()) {
//            String[] line = scanner.nextLine().split("\\s+");
//            System.err.println(StringUtils.join(line, "\t"));
//            setEnv(line[0], line[1]);
//        }
    }

    @Before
    public void cleanIRODS() throws JargonException, FileNotFoundException {
        setEnv();

        String[] args = {"-values", "./src/test/resources/upload/TEST1.txt", "--push", "--irods", "/unlock/home/koehorst"};

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);

        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder").delete();
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder").delete();
    }

    // - Push a local file to an existing remote location (create)
    @Test
    public void test1() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/TEST1.txt", "--push", "--irods", "/unlock/home/koehorst"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST1.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST1.txt").delete();
    }

    // - Push a local file to a non-existing file location (rename)
    @Test
    public void test2() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/TEST1.txt", "--push", "--irods", "/unlock/home/koehorst/TESTRename.txt"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRename.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRename.txt").delete();
    }


    // - Push multiple local files to an existing remote folder location
    @Test
    public void test3() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/TEST1.txt", "./src/test/resources/upload/TEST2.txt", "--push", "--irods", "/unlock/home/koehorst/"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST2.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST1.txt").delete();
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TEST2.txt").delete();
    }

    // - Push multiple local files to a non-existing remote folder location (create path)
    @Test
    public void test4() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/TEST1.txt", "./src/test/resources/upload/TEST2.txt", "--push", "--irods", "/unlock/home/koehorst/NONEXISTING"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/NONEXISTING/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/NONEXISTING/TEST2.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/NONEXISTING/TEST1.txt").delete();
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/NONEXISTING/TEST2.txt").delete();
    }

    // - Push a single local folder to an existing remote location (create)
    @Test
    public void test5() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload", "--push", "--irods", "/unlock/home/koehorst"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/upload/TEST1.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/upload").delete();
    }

    // - Push a single local folder to an existing remote location with the same name (upload content)
    @Test
    public void test5b() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload", "--push", "--irods", "/unlock/home/koehorst/upload"};
        App.main(args);

//        CommandOptions commandOptions = new CommandOptions(args);
//        Connection connection = new Connection(commandOptions);
//        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/upload/TEST1.txt").isFile());
//        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/upload").delete();
    }

    // - Push a single local folder to a remote non-existing folder location (rename)
    @Test
    public void test6() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload", "--push", "--irods", "/unlock/home/koehorst/TESTRENAMEFOLDER"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRENAMEFOLDER/upload/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRENAMEFOLDER/upload/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRENAMEFOLDER/upload/secondFolder/TEST1.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/TESTRENAMEFOLDER").delete();
    }

    //- Push multiple folders to a remote existing folder location (create)
    @Test
    public void test7() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/firstFolder", "./src/test/resources/upload/secondFolder", "--push", "--irods", "/unlock/home/koehorst/"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder/TEST1.txt").isFile());

        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder").delete();
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder").delete();
    }

    //- Push multiple folders to a remote non-existing folder location (create)
    @Test
    public void test8() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/firstFolder", "./src/test/resources/upload/secondFolder", "--push", "--irods", "/unlock/home/koehorst/new"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/new/firstFolder/TEST1.txt").isFile());
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/new/secondFolder/TEST1.txt").isFile());

        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/new").delete();
    }

    // Duplicate entry
    @Test
    public void test9() throws FileAlreadyExistsException, JargonException, GenQueryBuilderException, JargonQueryException, FileNotFoundException {
        String[] args = {"-values", "./src/test/resources/upload/firstFolder", "./src/test/resources/upload/firstFolder", "--push", "--irods", "/unlock/home/koehorst/"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder/TEST1.txt").isFile());

        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder").delete();
        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder").delete();
    }

    @Test
    public void testYodaAlternative() throws JargonException {
        String[] args = {
                "--values", "/Users/jasperk/test",
                "--push",
                "--irods", "/unlock/home/koehorst/",
                "--force"
        };
        App.main(args);
    }


    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }
}
