import nl.wur.ssb.App;
import nl.wur.ssb.CommandOptions;
import nl.wur.ssb.Connection;
import org.apache.commons.io.FileUtils;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.junit.Assert;
import org.junit.Before;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.FileAlreadyExistsException;
import java.util.Map;

public class Test {



    // - Push a local file to an existing remote location (create)
    @org.junit.Test
    public void test1() throws FileAlreadyExistsException, JargonException, FileNotFoundException, GenQueryBuilderException, JargonQueryException {
        // PullTest.setEnv();

        String[] args = {"--files", "/Users/jasperkoehorst/GitLab/m-unlock/cwl/WORKFLOW", "--push", "--irods", "/tempZone/Projects/P_NWO_unlock_test/I_Investigation_Identifier/S_Study_Identifier/O_ObservationUnit_1/Spades_test_run_new_yaml"};
        App.main(args);

        CommandOptions commandOptions = new CommandOptions(args);
        Connection connection = new Connection(commandOptions);
        Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/tempZone/home/koehorst/TEST1.txt").isFile());
        connection.fileFactory.instanceIRODSFile("/tempZone/home/koehorst/TEST1.txt").delete();
    }
}
