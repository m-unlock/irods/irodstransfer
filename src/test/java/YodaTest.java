import nl.wur.ssb.App;
import nl.wur.ssb.CommandOptions;
import nl.wur.ssb.Connection;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.nio.file.FileAlreadyExistsException;
import java.util.Map;

public class YodaTest {

    @Test
    public void testYoda() throws JargonException {
        // java -jar IRODSTransfer.jar
        // --host wur-yoda.irods.surfsara.nl
        // --irods /wur/home/research-ssb-projects-example/
        // --password vanrar-tixsi8-jotcoX
        // --username jasper.koehorst@wur.nl --local ..... --sslpolicy CS_DONT_CARE
        String[] args = {
                "--host", "wur-yoda.irods.surfsara.nl",
                "--port", "1247",
                "--zone", "wur",
                "--force",
                "--authentication", "pam",
                
                "--sslpolicy", "CS_NEG_DONT_CARE",
                "--values", "/Users/jasperk/test",
                "--push",
                "--threads", "1",
                "--irods", "/wur/home/research-ssb-projects-example"
        };

        App.main(args);

//        CommandOptions commandOptions = new CommandOptions(args);
//        Connection connection = new Connection(commandOptions);
        // Assert.assertEquals(true, connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder/TEST1.txt").isFile());

//        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/firstFolder").delete();
//        connection.fileFactory.instanceIRODSFile("/unlock/home/koehorst/secondFolder").delete();
    }
}
